function validateForm() {
	var nameInput = document.forms["contactUsForm"]["name"].value;
	var emailInput = document.forms["contactUsForm"]["email"].value;
	var phoneInput = document.forms["contactUsForm"]["phone"].value;
	var moreInfo = document.forms["contactUsForm"]["additionalInfo"].value;
	var mondayContact = document.forms["contactUsForm"]["mon"].checked;
	var tuesdayContact = document.forms["contactUsForm"]["tue"].checked;
	var wednesdayContact = document.forms["contactUsForm"]["wed"].checked;
	var thursdayContact = document.forms["contactUsForm"]["thu"].checked;
	var fridayContact = document.forms["contactUsForm"]["fri"].checked;


	if (nameInput == null || nameInput == "") {
		alert("Your name, please!");
		return false;
	}

	if (emailInput == "" && phoneInput == "") {
		alert("Either your phone number or email address for us, please!");
		return false;
	}
	
	if (document.getElementById("other").selected == true) {
		if (moreInfo == "") {
			alert("Additional information for selecting the \"Other\" option, please!");
			return false;
		}
	}

	if (mondayContact == false && tuesdayContact == false && wednesdayContact == false && thursdayContact == false && fridayContact == false) {
		alert("The best day(s) to contact you, please!");
		return false;
	}
}
